module.exports = {
    BAMBOO_HR_USER_ID: 'YOUR_BAMBOO_USER_ID',
    BAMBOO_HR_SESSION_ID: 'YOUR_BAMBOO_SESSION_COOKIE',
    BAMBOO_HR_ENTRIES_URL: 'https://epitech.bamboohr.com/timesheet/clock/entries',
    BAMBOO_HR_TIMESHEET_START_DATE: 'TIMESHEET_START_DATE',
    BAMBOO_HR_TIMESHEET_END_DATE: 'TIMESHEET_END_DATE',
    BAMBOO_HR_TIMESHEET_URL: 'https://epitech.bamboohr.com/employees/timesheet/?id='
};
