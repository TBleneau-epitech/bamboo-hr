module.exports = {
    Monday: {
        AM: {
            start: '09:00',
            end: '12:00'
        },
        PM: {
            start: '13:00',
            end: '18:00'
        }
    },
    Tuesday: {
        AM: {
            start: '09:00',
            end: '12:00'
        },
        PM: {
            start: '13:00',
            end: '18:00'
        }
    },
    Wednesday: {
        AM: {
            start: '09:00',
            end: '12:00'
        },
        PM: {
            start: '13:00',
            end: '18:00'
        }
    },
    Thursday: {
        AM: {
            start: '09:00',
            end: '12:00'
        },
        PM: {
            start: '13:00',
            end: '18:00'
        }
    },
    Friday: {
        AM: {
            start: '09:00',
            end: '12:00'
        },
        PM: {
            start: '13:00',
            end: '17:00'
        }
    }
}
