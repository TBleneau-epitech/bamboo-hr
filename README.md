# BAMBOO HR - Automating information on arrival and departure times at the office

## Getting Started

During my time at Epitech, we had to connect to the bamboohr platform to clock in and clock out of the office every day.
Since many people forget to do this time-consuming process, I decided to create a NodeJS script.

## Add your configuration file

```
cd config
cp bamboo.example.js bamboo.js
```

You juste have to open the newly copied file and edit it.
Simply enter your user ID, your session cookie (when you log in to bamboo hr) and the start and end date of the pay period.

## Launch the script

Warning ! You must have NodeJS installed on your work environment.
To launch the script, run the following commands:

```
    npm install
    node main.js
```
