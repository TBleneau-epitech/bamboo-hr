const got = require('got');
const {CookieJar} = require('tough-cookie');
const bambooHr = require('./config/bamboo');
const days = require('./config/entries');


function formatDate(date) {
    return date.getFullYear() + '-' + String(date.getMonth() + 1).padStart(2, '0') + '-' + String(date.getDate()).padStart(2, '0');
}

function getFormattedEntriesByDay(date, day) {
    return [
        {
            date: date,
            employeeId: bambooHr.BAMBOO_HR_USER_ID,
            id: null,
            note: '',
            projectId: null,
            taskId: null,
            trackingId: 1,
            ...days[day].AM
        },
        {
            date: date,
            employeeId: bambooHr.BAMBOO_HR_USER_ID,
            id: null,
            note: '',
            projectId: null,
            taskId: null,
            trackingId: 1,
            ...days[day].PM
        }
    ];
}

function getFormattedEntries() {
    let entries = {entries: []};
    let endDate = new Date(bambooHr.BAMBOO_HR_TIMESHEET_END_DATE);
    const days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];

    for (let date = new Date(bambooHr.BAMBOO_HR_TIMESHEET_START_DATE); date <= endDate; date.setDate(date.getDate() + 1)) {
        if (days[date.getDay()] !== 'Sunday' && days[date.getDay()] !== 'Saturday') {
            entries.entries = [...entries.entries, ...getFormattedEntriesByDay(formatDate(date), days[date.getDay()])];
        }
    }
    return entries;
}

function createCookieJar() {
    const cookieJar = new CookieJar();

    cookieJar.setCookie(`PHPSESSID=${bambooHr.BAMBOO_HR_SESSION_ID}`, 'https://epitech.bamboohr.com');
    return cookieJar;
}

async function getCSRFToken(url, cookieJar) {
    return got(url, {cookieJar});
}

async function updateSchedule(cookieJar, token, entries) {
    let options;

    options = {method: 'POST', json: entries, headers: {'x-csrf-token': token}, cookieJar}
    got(bambooHr.BAMBOO_HR_ENTRIES_URL, options)
        .then(() => console.log('The entries were added to the timesheet !'))
        .catch(error => console.error(`${error} - An error occurred during the update of the timesheet !`));
}

function main() {
    const cookieJar = createCookieJar();

    getCSRFToken(bambooHr.BAMBOO_HR_TIMESHEET_URL + bambooHr.BAMBOO_HR_USER_ID, cookieJar)
        .then(response => {
            let regex = /(?<=var CSRF_TOKEN = ")(.*)(?=";)/g
            let token = regex.exec(response.body).splice(1).pop();

            (token) ? updateSchedule(cookieJar, token, getFormattedEntries()) : console.error('An error occurred during the request for the CSRF token !');
        })
        .catch(error => console.error(`${error} - An error occurred during the request for the CSRF token !`));
}

main();
